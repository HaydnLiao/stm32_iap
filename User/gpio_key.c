
#include "gpio_key.h"

void Gpio_Key_Init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
	GPIO_InitTypeDef gpioInit;
	gpioInit.GPIO_Pin = GPIO_Pin_4;		// KEY0
	gpioInit.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOE, &gpioInit);
	gpioInit.GPIO_Pin = GPIO_Pin_3;
	GPIO_Init(GPIOE, &gpioInit);			// KEY1
}

uint8_t Key0_GetStatus(void)
{
	// 按键低电平有效
	return (GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_4) == Bit_SET) ? 0 : 1;
}

uint8_t Key1_GetStatus(void)
{
	// 按键低电平有效
	return (GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_3) == Bit_SET) ? 0 : 1;
}
