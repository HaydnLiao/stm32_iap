
#ifndef GPIO_KEY_H
#define GPIO_KEY_H

#include "stm32f10x.h"

void Gpio_Key_Init(void);
uint8_t Key0_GetStatus(void);
uint8_t Key1_GetStatus(void);

#endif
