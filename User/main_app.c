/**
  ******************************************************************************
  * @file    User/main_app.c 
  * @date    11-Dec-2017
  * @brief   Main program body
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "usart1_printf.h"
#include "systick_delay.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define IAP_ADDRESS			0x8000000		// 16 KB
#define APP_ADDRESS			0x8004000		// 48 KB
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void)
{
	Systick_Delay_Init();
	Usart1_Printf_Init();

	NVIC_SetVectorTable(NVIC_VectTab_FLASH, APP_ADDRESS - IAP_ADDRESS);
	
  /* Infinite loop */
  while (1)
  {
		printf("OK\r\n");
		delay_ms(1000);
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
