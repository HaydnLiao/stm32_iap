/**
  ******************************************************************************
  * @file    User/usart2_bt.c 
  * @date    13-Dec-2017
  * @brief   USART2 config functions and bluetooth send data function
  ******************************************************************************
  */ 

#include "usart2_bt.h"

uint8_t bt_rx_buff[BT_RX_BUFF_LEN] = {0};
uint16_t bt_rx_len = 0;
uint8_t bt_rc_flag = 0;
uint8_t bt_tx_buff[BT_TX_BUFF_LEN] = {0};

/**
  * @brief  初始化USART2
  * @param  None
  * @retval None
  */
void Usart2_Bt_Init(void)
{
	Usart2_Config();
	Nvic_Config();
	// 使能串口接收中断
	USART_ITConfig(BT_USART, USART_IT_RXNE, ENABLE);	
	USART_ITConfig(BT_USART, USART_IT_IDLE, ENABLE);
  // 清除发送完成标志
	//USART_ClearFlag(BT_USART, USART_FLAG_TC);
}
/**
  * @brief  USART2引脚配置及串口配置
  * @param  None
  * @retval None
  */
static void Usart2_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	// 打开串口GPIO的时钟
	BT_GPIO_CLK_CMD(BT_GPIO_CLK, ENABLE);
	
	// 打开串口外设的时钟
	BT_USART_CLK_CMD(BT_USART_CLK, ENABLE);

	// 将USART Tx的GPIO配置为推挽复用模式
	GPIO_InitStructure.GPIO_Pin = BT_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(BT_TX_PORT, &GPIO_InitStructure);

  // 将USART Rx的GPIO配置为浮空输入模式
	GPIO_InitStructure.GPIO_Pin = BT_RX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(BT_RX_PORT, &GPIO_InitStructure);
	
	// 配置串口的工作参数
	// 配置波特率
	USART_InitStructure.USART_BaudRate = BT_USART_BAUDRATE;
	// 配置帧数据字长
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	// 配置停止位
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	// 配置校验位
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	// 配置硬件流控制
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	// 配置工作模式，收发一起
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	// 完成串口的初始化配置
	USART_Init(BT_USART, &USART_InitStructure);
	// 使能串口
	USART_Cmd(BT_USART, ENABLE);

	GPIO_InitStructure.GPIO_Pin = BT_STATUS_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(BT_STATUS_PORT, &GPIO_InitStructure);
}
/**
  * @brief  USART2中断优先级配置
  * @param  None
  * @retval None
  */
static void Nvic_Config(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  
  /* 嵌套向量中断控制器组选择 */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
  
  /* 配置USART为中断源 */
  NVIC_InitStructure.NVIC_IRQChannel = BT_USART_IRQ;
  /* 抢断优先级*/
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
  /* 子优先级 */
  // NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  /* 使能中断 */
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  /* 初始化配置NVIC */
  NVIC_Init(&NVIC_InitStructure);
}

/**
  * @brief  USART2发送一个字节
  * @param  ch为发送的字节
  * @retval None
  */
void Bt_SendByte(uint8_t ch)
{
	/* 发送一个字节数据到USART */
	USART_SendData(BT_USART, (uint16_t)ch);
		
	/* 等待发送数据寄存器为空 */
	while (USART_GetFlagStatus(BT_USART, USART_FLAG_TXE) == RESET)
	{
	}
}
/**
  * @brief  USART2发送一帧数据
  * @param  str为要发送的数据数组的头指针，size为要发送数据的长度
  * @retval None
  */
void Bt_SendData(uint8_t *str, uint16_t size)
{
	uint16_t cnt = 0;
  for(; cnt < size; cnt++)
  {
      Bt_SendByte(*(str + cnt));
  } 
  
  /* 等待发送完成 */
  while(USART_GetFlagStatus(BT_USART, USART_FLAG_TC) == RESET)
  {
	}
}

/**
  * @brief  获取蓝牙连接状态
  * @param  None
  * @retval 返回蓝牙连接状态
  */
uint8_t Bt_GetConStatus(void)
{
	return GPIO_ReadInputDataBit(BT_STATUS_PORT, BT_STATUS_PIN);
}
/**
  * @brief  USART2中断处理函数
  * @param  None
  * @retval None
  */
void BT_USART_IRQHandler(void)
{
	if(USART_GetITStatus(BT_USART, USART_IT_RXNE) == SET)
	{	
		USART_ClearFlag(BT_USART, USART_FLAG_RXNE);
		bt_rx_buff[bt_rx_len] = (uint8_t)(USART_ReceiveData(BT_USART) & (uint8_t)0xff);
		bt_rx_len = (bt_rx_len + 1) % BT_RX_BUFF_LEN;
	}
	else if(USART_GetITStatus(BT_USART,USART_IT_IDLE) == SET)
	{
		// 清空闲中断
		BT_USART->SR;
		BT_USART->DR;
		bt_rx_buff[bt_rx_len] = 0;
		bt_rc_flag = 1;
	}
}
/**
  ******************************************************************************
  * @file    User/usart2_bt.h 
  * @date    13-Dec-2017
  * @brief   all the functions prototypes for USART2
  ******************************************************************************
  */ 

#ifndef __USART2_BT_H
#define __USART2_BT_H

#include "stm32f10x.h"

#define BT_USART						USART2
#define BT_USART_CLK				RCC_APB1Periph_USART2
#define BT_USART_CLK_CMD		RCC_APB1PeriphClockCmd
#define BT_USART_BAUDRATE		115200

#define BT_USART_IRQ				USART2_IRQn
#define BT_USART_IRQHandler	USART2_IRQHandler

#define BT_TX_PORT					GPIOA
#define BT_TX_PIN						GPIO_Pin_2
#define BT_RX_PORT					GPIOA
#define BT_RX_PIN						GPIO_Pin_3
#define BT_STATUS_PORT			GPIOA
#define BT_STATUS_PIN				GPIO_Pin_4
#define BT_GPIO_CLK					RCC_APB2Periph_GPIOA
#define BT_GPIO_CLK_CMD			RCC_APB2PeriphClockCmd

#define BT_RX_BUFF_LEN			4096
extern uint8_t bt_rx_buff[BT_RX_BUFF_LEN];
extern uint16_t bt_rx_len;
extern uint8_t bt_rc_flag;
#define BT_TX_BUFF_LEN			32
extern uint8_t bt_tx_buff[BT_TX_BUFF_LEN];

void Usart2_Bt_Init(void);
static void Usart2_Config(void);
static void Nvic_Config(void);
void Bt_SendByte(uint8_t ch);
void Bt_SendData(uint8_t *str, uint16_t size);
uint8_t Bt_GetConStatus(void);

#endif
