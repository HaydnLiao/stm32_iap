/**
  ******************************************************************************
  * @file    User/usart2_bt.h 
  * @date    13-Dec-2017
  * @brief   all the functions prototypes for USART2
  ******************************************************************************
  */ 

#ifndef __USART2_BT_H
#define __USART2_BT_H

#include "stm32f10x.h"

#define BT_USART						USART2
#define BT_USART_CLK				RCC_APB1Periph_USART2
#define BT_USART_CLK_CMD		RCC_APB1PeriphClockCmd
#define BT_USART_BAUDRATE		115200

#define BT_USART_IRQ				USART2_IRQn
#define BT_USART_IRQHandler	USART2_IRQHandler

#define BT_TX_PORT					GPIOA
#define BT_TX_PIN						GPIO_Pin_2
#define BT_RX_PORT					GPIOA
#define BT_RX_PIN						GPIO_Pin_3
#define BT_STATUS_PORT			GPIOA
#define BT_STATUS_PIN				GPIO_Pin_4
#define BT_GPIO_CLK					RCC_APB2Periph_GPIOA
#define BT_GPIO_CLK_CMD			RCC_APB2PeriphClockCmd

#define BT_RX_BUFF_LEN			4096
extern uint8_t bt_rx_buff[BT_RX_BUFF_LEN];
extern uint16_t bt_rx_len;
extern uint8_t bt_rc_flag;
#define BT_TX_BUFF_LEN			32
extern uint8_t bt_tx_buff[BT_TX_BUFF_LEN];

void Usart2_Bt_Init(void);
static void Usart2_Config(void);
static void Nvic_Config(void);
void Bt_SendByte(uint8_t ch);
void Bt_SendData(uint8_t *str, uint16_t size);
uint8_t Bt_GetConStatus(void);

#endif
